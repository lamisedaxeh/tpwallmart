# Les threads en C++
Petit projet pour mieux comprendre les performances des threads réalisé en C++.
# Génération plot avec GNU Plot
Set des titres des axes


> ` gnuplot> set xlabel "M le nombre d'itération"`

> ` gnuplot> set ylabel "Tn en ms"`


Génération du plot avec le Temps exécution monothread et Temps exécution multithread

> ` gnuplot> plot "monthreadFinal.txt" with lines title "Temps exécution monothread" , "multithreadFinal.txt" with lines title "Temps exécution multithread"`


Génération du plot avec le Temps exécution monothread, Temps exécution multithread et Temps création thread


> `plot "monthreadFinal.txt" with lines title "Temps exécution monothread", "multithreadFinal.txt" u 1:2 with lines title "Temps exécution multithread", "" u 1:3 with lines title "Temps création thread"`
