#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <pthread.h>
#include <algorithm>
#include <initializer_list>

using namespace std;

double tpsMoyenne = 0;
double tpsMoyenneQuadra =0 ;
double tpsSommeCube = 0;
double tpsCreationThread = 0;

/**
 * structure client
 *
 */
typedef struct Client
{
	// tab d'article
	int * tab;
	//Nombre d'article
	int N;
}Client;




/**
 * Rempli aléatoiremenet entre 1 et 10 un tableau
 * @tab tableau à remplire
 * @N taille max du tableau
 */
/*void* acheter(void* C)
{
	Client *client;
	client=(Client*)C;
	for (int i = 0; i<client->N;i++)
	{
		client->tab[i] = rand() % 10 + 1;	
	}
	pthread_exit((void*)client);
}*/


/**
 * Rempli aléatoiremenet entre 1 et 10 un tableau
 * @tab tableau à remplire
 * @N taille max du tableau
 */
void acheter(int* tab,int N)
{
	for (int i = 0; i<N;i++)
	{
		tab[i] = rand() % 10 + 1;	
	}
}

/**
 * Calcule la moyenne de tab
 * @tab tableau d'input
 * @N taille max du tableau
 */
void* moyenne(void* C){
	clock_t tempsTotal = clock();
	Client *client;
	client=(Client*)C;
	double* moyenne = (double*)malloc(sizeof(double));
	for (int i = 0; i<client->N;i++)
	{
		(*moyenne) += client->tab[i];
	}
	*moyenne = *moyenne/client->N;
//	cout << "Temps moyenne " << (double)(clock()-tempsTotal)/(CLOCKS_PER_SEC/1000)<<endl;
	tpsMoyenne += (double)(clock()-tempsTotal)/(CLOCKS_PER_SEC/1000);
	pthread_exit((void*)moyenne);

}

/**
 * Calcule la moyenne quadratique de tab
 * @tab tableau d'input
 * @N taille max du tableau
 */
void* moyenneQuadra(void* C){
	clock_t tempsTotal = clock();
	Client *client;
	client=(Client*)C;
	double* sommexi = (double*)malloc(sizeof(double));
	for (int i = 0; i<client->N;i++)
	{
		*sommexi += client->tab[i] * client->tab[i];
	}
	*sommexi = sqrt(*sommexi/client->N);
//	cout << "Temps moyenne quadra" << (double)(clock()-tempsTotal)/(CLOCKS_PER_SEC/1000)<<endl;
	tpsMoyenneQuadra += (double)(clock()-tempsTotal)/(CLOCKS_PER_SEC/1000);
	
	pthread_exit((void*)sommexi);

}

/**
 * Calcule la somme des cube de tab
 * @tab tableau d'input
 * @N taille max du tableau
 */
void * sommeCube(void* C){
	clock_t tempsTotal = clock();
	Client *client;
	client=(Client*)C;
	double* somme = (double*)malloc(sizeof(double));
	for (int i = 0; i<client->N;i++)
	{
		*somme += client->tab[i] * client->tab[i] * client->tab[i];
	//	cout << client->tab[i];
	}
	//cout << "Temps sommecube " << (double)(clock()-tempsTotal)/(CLOCKS_PER_SEC/1000)<<endl;
	tpsSommeCube += (double)(clock()-tempsTotal)/(CLOCKS_PER_SEC/1000);
	pthread_exit((void*)somme);

}


/**
 * Calcule le totale de la caisse.
 */
double* caisse(Client client, double* ticket){

	
	pthread_t T1,T2,T3;
	clock_t tempsTotal = clock();
	pthread_create(&T1,NULL,moyenne,&client);
	pthread_create(&T2,NULL,moyenneQuadra,&client);
	pthread_create(&T3,NULL,sommeCube,&client);
//	cout << "Temps création thread" << (double)(clock()-tempsTotal)/(CLOCKS_PER_SEC/1000)<<endl;
	tpsCreationThread += (double)(clock()-tempsTotal)/(CLOCKS_PER_SEC/1000);


	double* moyenne;
	double* sommexi;
	double* somme;
	pthread_join(T1,(void**)&moyenne);
	pthread_join(T2,(void**)&sommexi);
	pthread_join(T3,(void**)&somme);
	ticket[0] = (*moyenne);
	ticket[1] = *sommexi;
	ticket[2] = *somme;


/*	ticket[0] = moyenne (tab,N);
	ticket[1] = moyenneQuadra(tab,N);
	ticket[2] = sommeCube(tab,N);*/
	return ticket; 
}

int main() 
{
	
	srand(time(NULL));
//	int const M = 15;

	clock_t tempsAchat;

	double ticket[3];
	//init tab
	int * tab;
	//init tab
	int N = 4;
		
	tab = (int*) malloc(sizeof(int) * N);

	//Verif bonne init du tab
	if (tab==NULL) exit (1);	


	Client client;
	
	client.N = N;
	client.tab = tab;

	clock_t tempsTotal;
	for(int M=1; M<2000;M++){
		tempsTotal = clock();
		tpsMoyenne = 0;
		tpsMoyenneQuadra =0 ;
		tpsSommeCube = 0;
		tpsCreationThread = 0;

		for(int j = 0; j<M;j++)
		{

			//	cout << "CLIENT : " << j << endl;



			//test de archeter/
			acheter(tab,N); 
			//	cout << "Temps Achat : " << (double)(clock()-tempsAchat)/(CLOCKS_PER_SEC/1000);

			//	cout << "    TAB : "<< endl;
			/*			for (int i = 0; i<N;i++){
						cout << "         tab "<< i << " :" << tab[i] << endl;
						}*/
			//			cout << "    moyenne : " << caisse(tab,N,ticket) << endl;
			caisse(client,ticket);
		}
		cout << M << "	" << (double)(clock()-tempsTotal)/(CLOCKS_PER_SEC/1000)<< "	" /*<< max({tpsMoyenne, tpsMoyenneQuadra, tpsSommeCube}) << "	"*/ <<  tpsCreationThread <<endl;

		//	cout << M << "	" << (double)(clock()-tempsTotal)/(CLOCKS_PER_SEC/1000)<< "	" << tpsMoyenne << "	" << tpsMoyenneQuadra << "	" << tpsSommeCube << "	" <<  tpsCreationThread <<endl;
	}


	return 0;
}
