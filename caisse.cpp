#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

using namespace std;

double tpsExe = 0;

/**
 * Rempli aléatoiremenet entre 1 et 10 un tableau
 * @tab tableau à remplire
 * @N taille max du tableau
 */
void acheter(int* tab,int N)
{
	for (int i = 0; i<N;i++)
	{
		tab[i] = rand() % 10 + 1;	
	}
}

/**
 * Calcule la moyenne de tab
 * @tab tableau d'input
 * @N taille max du tableau
 */
double moyenne(int * tab, int N){
	double moyenne = 0;
	for (int i = 0; i<N;i++)
	{
		moyenne += tab[i];
	}
	moyenne = moyenne/N;
	return moyenne;
}

/**
 * Calcule la moyenne quadratique de tab
 * @tab tableau d'input
 * @N taille max du tableau
 */
double moyenneQuadra(int * tab, int N){
	double sommexi = 0;
	for (int i = 0; i<N;i++)
	{
		sommexi += tab[i] * tab[i];
	}
	sommexi = sqrt(sommexi/N);
	return sommexi;
}

/**
 * Calcule la somme des cube de tab
 * @tab tableau d'input
 * @N taille max du tableau
 */
double sommeCube(int * tab, int N){
	double somme = 0;
	for (int i = 0; i<N;i++)
	{
		somme += tab[i] * tab[i] * tab[i];
	//	cout << tab[i];
	}
	return somme;
}


/**
 * Calcule le totale de la caisse.
 */
double* caisse(int* tab, int N, double* ticket){
	clock_t tempsTotal = clock();
	ticket[0] = moyenne (tab,N);
	ticket[1] = moyenneQuadra(tab,N);
	ticket[2] = sommeCube(tab,N);
	tpsExe += (double)(clock()-tempsTotal)/(CLOCKS_PER_SEC/1000);
	return ticket; 
}

int main() 
{
	
	srand(time(NULL));
//	int const M = 15;

	clock_t tempsAchat;

	double ticket[3];
	//init tab
	int * tab;
	//init tab
	int N = 4;
		
	tab = (int*) malloc(sizeof(int) * N);

	//Verif bonne init du tab
	if (tab==NULL) exit (1);	

	clock_t tempsTotal;
	for(int M=1; M<2000;M++){
		tempsTotal = clock();
		tpsExe = 0;
		for(int j = 0; j<M;j++)
		{

			//	cout << "CLIENT : " << j << endl;



			//test de archeter/
			tempsAchat = clock();
			acheter(tab,N); 
			//	cout << "Temps Achat : " << (double)(clock()-tempsAchat)/(CLOCKS_PER_SEC/1000);

			//	cout << "    TAB : "<< endl;
/*			for (int i = 0; i<N;i++){
				cout << "         tab "<< i << " :" << tab[i] << endl;
			}*/
			//			cout << "    moyenne : " << caisse(tab,N,ticket) << endl;
			caisse(tab,N,ticket);
		}
	cout << M << "    " << (double)(clock()-tempsTotal)/(CLOCKS_PER_SEC/1000) /*<< "	" << tpsExe*/ <<endl;
	}


	return 0;
}
